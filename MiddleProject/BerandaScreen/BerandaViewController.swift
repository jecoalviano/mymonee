//
//  BerandaViewController.swift
//  MiddleProject
//
//  Created by Jeco Alviano on 12/05/21.
//

import UIKit

class BerandaViewController: UIViewController{
    
    @IBOutlet weak var loading: UIActivityIndicatorView!
    var income: Int = 0
    var outcome: Int = 0
    
    enum TimeType: String{
        case Pagi = "Selamat Pagi,"
        case Siang = "Selamat Siang,"
        case Sore = "Selamat Sore,"
        case Malam = "Selamat Malam,"
    }
    
    @IBOutlet weak var mainTableView: UITableView!{
        didSet {
            //tableview
            mainTableView.delegate = self
            mainTableView.dataSource = self
            let uiNib = UINib(nibName: String(describing: TableViewCellPengguna.self), bundle: nil)
            mainTableView.register(uiNib, forCellReuseIdentifier: String(describing: TableViewCellPengguna.self))
        }
    }
    
    @IBOutlet weak var saldoView: UIView!
    @IBOutlet weak var tabBarView: UIView!
    @IBOutlet weak var mainCollectionView: UICollectionView!{
        didSet{
            //collectionview
            mainCollectionView.delegate = self
            mainCollectionView.dataSource = self
            let collection = UINib(nibName: String(describing: CollectionViewCell.self), bundle: nil)
            mainCollectionView.register(collection, forCellWithReuseIdentifier: String(describing: CollectionViewCell.self))
        }
    }
    
    
    @IBOutlet weak var greetings: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var totalSaldo: UILabel!
    @IBOutlet weak var notFoundImage: UIImageView!
    @IBOutlet weak var norFoundWords: UIImageView!

    var transactionList: [TableViewContents] = [] {
        didSet
        {
            mainTableView.reloadData()
        }
    }
    
    var service: BerandaNetworkService = BerandaNetworkService()
    
    func loadData() {
        service.backendMonee { transaction in
            DispatchQueue.main.async {
                self.loading.startAnimating()
                self.transactionList = transaction
                var total = 0
                
                self.transactionList.forEach { (results) in
                    
                    if results.status == true{
                        self.income += results.contentsPrice ?? 0
                    }
                    if results.status == false{
                        self.outcome += results.contentsPrice ?? 0
                    }
                    total = self.income - self.outcome
                }
                self.mainCollectionView.reloadData()
                self.totalSaldo.text = "Rp \(NumberFormatter().getFormat(total))"
                self.loading.stopAnimating()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.loadData()
        
        if let savedData = UserDefaults.standard.value(forKey: "savedArray") as? Data {
            let _riwayatPenggunaan = try? PropertyListDecoder().decode(Array<TableViewContents>.self, from: savedData)
            cellcontents = _riwayatPenggunaan ?? []
        }
        
//        total()
//        hitungIncome()
//        hitungOutcome()
        getTime()
        
        notFoundImage.isHidden = true
        norFoundWords.isHidden = true
        
        name.text = user[0].name
        
        
        mainCollectionView.layer.cornerRadius = 5
        saldoView.layer.cornerRadius = 10
        tabBarView.layer.cornerRadius = 10
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        name.text = user[0].name
//        total()
        mainTableView.reloadData()
        getTime()
    }

    
    @IBAction func btnAdd(_ sender: Any) {
        let add = TambahPenggunaanNavigationController(nibName: String(describing: TambahPenggunaanNavigationController.self), bundle: nil)
        self.navigationController?.pushViewController(add, animated: true)
    }
    
    func getTime(){
        let date = Date()
        let formatter = DateFormatter()
        formatter.timeZone = .current
        formatter.locale = .current
        formatter.dateFormat = "HH"
        let time = formatter.string(from: date)
        
        if(Int(time)! > 4 && Int(time)! < 10){
            greetings.text = TimeType.Pagi.rawValue
        }
        if(Int(time)! > 10 && Int(time)! < 14){
            greetings.text = TimeType.Siang.rawValue
        }
        if(Int(time)! > 14 && Int(time)! < 18){
            greetings.text = TimeType.Sore.rawValue
        }
        if(Int(time)! > 18 && Int(time)! < 24 || Int(time)! < 4){
            greetings.text = TimeType.Malam.rawValue
        }
    }
/*
    func hitungIncome(){
        for i in 0..<cellcontents.count{
            if(cellcontents[i].status!){
                self.income += cellcontents[i].contentsPrice!
            }
        }
        print(income)
    }
    
    func hitungOutcome(){
        for i in 0..<cellcontents.count{
                if(cellcontents[i].status == false){
                    self.outcome += cellcontents[i].contentsPrice!
                    
                }
        }
}
    
    func total(){
        total = income - outcome
        totalSaldo.text = "Rp " + NumberFormatter().getFormat(total)
    }
*/
}
    
extension BerandaViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let viewController = LihatPenggunaanViewController(nibName: String(describing: LihatPenggunaanViewController.self), bundle: nil)

        viewController.index = indexPath.row
        print(indexPath.row)
/*
        viewController.id = cellcontents[indexPath.row].id ?? ""
        viewController.nama = cellcontents[indexPath.row].contentsName ?? ""
        viewController.tipe = cellcontents[indexPath.row].contentsType ?? ""
        viewController.tanggal = cellcontents[indexPath.row].contentsDate ?? ""
        viewController.total = cellcontents[indexPath.row].contentsPrice ?? 0
        viewController.status = cellcontents[indexPath.row].status ?? true
*/
        //from api
        viewController.id = transactionList[indexPath.row].id ?? ""
        viewController.nama = transactionList[indexPath.row].contentsName ?? ""
        viewController.tipe = transactionList[indexPath.row].contentsType ?? ""
        viewController.tanggal = transactionList[indexPath.row].contentsDate ?? ""
        viewController.total = transactionList[indexPath.row].contentsPrice ?? 0
        viewController.status = transactionList[indexPath.row].status ?? true

        self.navigationController?.pushViewController(viewController, animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
/*
        if cellcontents.count == 0{
            notFoundImage.isHidden = false
            norFoundWords.isHidden = false
            tableView.isHidden = true
        }else
        {
            notFoundImage.isHidden = true
            norFoundWords.isHidden = true
            tableView.isHidden = false
        }
        return cellcontents.count
*/
        if transactionList.count == 0{
            notFoundImage.isHidden = false
            norFoundWords.isHidden = false
            tableView.isHidden = true
        }else
        {
            notFoundImage.isHidden = true
            norFoundWords.isHidden = true
            tableView.isHidden = false
        }
        return transactionList.count
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TableViewCellPengguna.self), for: indexPath)
        as! TableViewCellPengguna

//        cell.lblJenis.text = cellcontents[indexPath.row].contentsName
//        cell.lblTanggal.text = cellcontents[indexPath.row].contentsDate
//
//        if cellcontents[indexPath.row].status == false{
//            cell.lblTotal.text = "-Rp " + NumberFormatter().getFormat(cellcontents[indexPath.row].contentsPrice ?? 0)
//            cell.lblTotal.textColor = UIColor.red
//
//            cell.ivPengguna.image = UIImage(systemName: "arrow.down" ,withConfiguration: UIImage.SymbolConfiguration(pointSize: 4, weight: .regular, scale: .small
//            ))
//            cell.ivPengguna.tintColor = UIColor.red
//            cell.ivPengguna.backgroundColor = UIColor.red.withAlphaComponent(0.1)
//
//        }
//        if cellcontents[indexPath.row].status == true{
//            cell.lblTotal.text = "+Rp " + NumberFormatter().getFormat(cellcontents[indexPath.row].contentsPrice ?? 0)
//            cell.lblTotal.textColor = UIColor.green
//
//            cell.ivPengguna.image = UIImage(systemName: "arrow.up", withConfiguration: UIImage.SymbolConfiguration(pointSize: 4, weight: .regular, scale: .small
//            ))
//            cell.ivPengguna.tintColor = UIColor.green
//            cell.ivPengguna.backgroundColor = UIColor.green.withAlphaComponent(0.1)
//        }
//
//
        //get dari api
        cell.showData(transaction: transactionList[indexPath.row])
        
       
        return cell
    }
}
//extension BerandaViewController: UICollectionViewDelegate{
//    
//}

extension BerandaViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath:IndexPath) -> CGSize{
        let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
        let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
        let size:CGFloat = (collectionView.frame.size.width - space) / 2.0
        return CGSize(width: size, height: size)
        //return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collection.count
    }
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let coll = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: CollectionViewCell.self), for: indexPath) as! CollectionViewCell

        coll.lblType.text = collection[indexPath.row].type
        //coll.lblAmount.text = "Rp \(collection[indexPath.row].amount ?? 0)"
        if collection[indexPath.row].status == false{

            coll.lblAmount.text = "Rp " + NumberFormatter().getFormat(outcome)
            coll.imageView.image = UIImage(systemName: "arrow.down" ,withConfiguration: UIImage.SymbolConfiguration(pointSize: 20, weight: .regular, scale: .small
            ))
            coll.imageView.tintColor = UIColor.red
            
        }
        if collection[indexPath.row].status == true{
            
            coll.lblAmount.text = "Rp " + NumberFormatter().getFormat(income)
            coll.imageView.image = UIImage(systemName: "arrow.up", withConfiguration: UIImage.SymbolConfiguration(pointSize: 25, weight: .regular, scale: .small
            ))
            coll.imageView.tintColor = UIColor.green
        }

        return coll
    }
}
