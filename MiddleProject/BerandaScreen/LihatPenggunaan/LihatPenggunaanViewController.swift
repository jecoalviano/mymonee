//
//  EditViewController.swift
//  MiddleProject
//
//  Created by Jeco Alviano on 14/05/21.
//

import UIKit

class LihatPenggunaanViewController: UIViewController {

    @IBOutlet weak var viewTemplate: UIView!
    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var namaPenggunaan: UILabel!
    @IBOutlet weak var tipePenggunaan: UILabel!
    @IBOutlet weak var idPenggunaan: UILabel!
    @IBOutlet weak var tanggalPenggunaan: UILabel!
    @IBOutlet weak var totalPenggunaan: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    
    var id: String = ""
    var nama: String = ""
    var tipe: String = ""
    var tanggal: String = ""
    var status: Bool = false
    var total: Int = 0
    
    var index: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        viewTemplate.layer.cornerRadius = 20
        
        backButton.layer.cornerRadius = 15
        backButton.layer.borderWidth = 2
        backButton.layer.borderColor = UIColor.systemIndigo.cgColor

        idPenggunaan.text = "\(id)"
        namaPenggunaan.text = nama
        tipePenggunaan.text = tipe
        tanggalPenggunaan.text = tanggal
        
        
        if status == false {
            totalPenggunaan.text = "-Rp \(total)"
            totalPenggunaan.textColor = UIColor.red
            
            imageLogo.image = UIImage(systemName: "arrow.down" ,withConfiguration: UIImage.SymbolConfiguration(pointSize: 4, weight: .regular, scale: .small
            ))
            imageLogo.tintColor = UIColor.red
            imageLogo.backgroundColor = UIColor.red.withAlphaComponent(0.1)
            
        }
        else {
            totalPenggunaan.text = "+Rp \(total)"
            totalPenggunaan.textColor = UIColor.green
            
            imageLogo.image = UIImage(systemName: "arrow.up", withConfiguration: UIImage.SymbolConfiguration(pointSize: 4, weight: .regular, scale: .small
            ))
            imageLogo.tintColor = UIColor.green
            imageLogo.backgroundColor = UIColor.green.withAlphaComponent(0.1)
        }
        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnKembali(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnEdit(_ sender: Any) {
       
        let edit = UbahPenggunaanNavigationController(nibName: String(describing: UbahPenggunaanNavigationController.self), bundle: nil)
        edit.nama = nama
        edit.total = total
        edit.status = status
        //edit.index = index
        edit.index = Int(id)! //from api
        print("index",id)
        self.navigationController?.setViewControllers([edit], animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
