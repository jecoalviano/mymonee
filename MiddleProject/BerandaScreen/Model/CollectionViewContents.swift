//
//  CollectionViewContents.swift
//  MiddleProject
//
//  Created by Jeco Alviano on 12/05/21.
//

import Foundation

struct CollectionViewContents {
    
    var type: String?
    var amount: Int?
    var status: Bool?
}


var collection : [CollectionViewContents] = [
    
    CollectionViewContents(type: "Uang masuk", amount: 0, status: true),
    CollectionViewContents(type: "Uang keluar", amount: 0, status: false)
]
