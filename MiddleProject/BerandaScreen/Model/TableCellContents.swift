//
//  CellContents.swift
//  MiddleProject
//
//  Created by Jeco Alviano on 12/05/21.
//

import Foundation
//for backend

struct Backend: Codable{
    var page: Int?
    var results: [CellContents]?
}

//till here

struct CellContents: Codable{
    
    var id: String?
    var contentsName : String?
    var contentsDate : String?
    var contentsPrice : Int?
    var status: Bool?
    var contentsType : String?
    
    //part backend
    enum CodingKeys: String, CodingKey {
        case id
        case contentsName
        case contentsDate
        case contentsPrice
        case status
        case contentsType
    }
    //till here
}

protocol addPenggunaan {
    func add()
}


class Penggunaan {
    var penggunaan: CellContents
    init(penggunaan: CellContents){
        self.penggunaan = penggunaan
    }
    func add(){
        cellcontents.append(penggunaan)
    }
}


var cellcontents : [CellContents] = [
//    TableCellContents(id: "MM-1", contentsName: "Penghasilan", contentsDate: "30 September 2019 - 12:00", contentsPrice: 1500000, status: true, contentsType: "Pemasukan"),
//    TableCellContents(id: "MM-2", contentsName: "Jajan", contentsDate: "30 November 2019 - 16:00", contentsPrice: 200000, status: false, contentsType: "Pengeluaran")
]
