//
//  TableViewCellPengguna.swift
//  MiddleProject
//
//  Created by Jeco Alviano on 12/05/21.
//

import UIKit

class TableViewCellPengguna: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblTanggal: UILabel!
    @IBOutlet weak var lblJenis: UILabel!
    @IBOutlet weak var ivPengguna: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    //api
    func showData(transaction: TableViewContents){
        /*
         if cellcontents[indexPath.row].status == true{
             cell.lblTotal.text = "+Rp " + NumberFormatter().getFormat(cellcontents[indexPath.row].contentsPrice ?? 0)
             cell.lblTotal.textColor = UIColor.green
         */
        lblTanggal.text = transaction.contentsDate
        lblJenis.text = transaction.contentsName
        if transaction.contentsType == "Pemasukan"{
            lblTotal.text = "+Rp " + NumberFormatter().getFormat(transaction.contentsPrice ?? 0)
            lblTotal.textColor = UIColor.green
            ivPengguna.image = UIImage(systemName: "arrow.up", withConfiguration: UIImage.SymbolConfiguration(pointSize: 4, weight: .regular, scale: .small
            ))
            ivPengguna.tintColor = UIColor.green
            
        }else
        {
            lblTotal.text = "-Rp " + NumberFormatter().getFormat(transaction.contentsPrice ?? 0)
            lblTotal.textColor = UIColor.red
            ivPengguna.image = UIImage(systemName: "arrow.down", withConfiguration: UIImage.SymbolConfiguration(pointSize: 4, weight: .regular, scale: .small
            ))
            ivPengguna.tintColor = UIColor.red

        }
        
        
    }
    
}
