//
//  TambahPenggunaNavigationController.swift
//  MiddleProject
//
//  Created by Jeco Alviano on 14/05/21.
//

import UIKit


class TambahPenggunaanNavigationController: UIViewController  {
    

    @IBOutlet weak var imagePengeluaran: UIImageView!
    @IBOutlet weak var save: UIButton!
    @IBOutlet weak var inputJudul: UITextField!
    @IBOutlet weak var inputJumlah: UITextField!
    
    @IBOutlet weak var imagePemasukan: UIImageView!
    @IBOutlet weak var viewPengeluaran: UIView!
    @IBOutlet weak var viewPemasukan: UIView!
    
    var tapped : Bool = true
    var type : String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        save.layer.cornerRadius = 10
        save.layer.backgroundColor = UIColor.gray.cgColor
        save.isEnabled = false
        
        viewPemasukan.layer.shadowOpacity = 0.2
        viewPemasukan.layer.cornerRadius = 10
        
        viewPengeluaran.layer.shadowOpacity = 0.2
        viewPengeluaran.layer.cornerRadius = 10
        
        let tappedPemasukan = UITapGestureRecognizer(target: self, action: #selector(tapPemasukan(_:)))
        viewPemasukan.addGestureRecognizer(tappedPemasukan)
        
        let tappedPengeluaran = UITapGestureRecognizer(target: self, action: #selector(tapPengeluaran(_:)))
        viewPengeluaran.addGestureRecognizer(tappedPengeluaran)
        
        imagePemasukan.image = UIImage(systemName: "arrow.up" ,withConfiguration: UIImage.SymbolConfiguration(pointSize: 4, weight: .regular, scale: .small
        ))?.withTintColor(.green, renderingMode:.alwaysOriginal)

        imagePengeluaran.image = UIImage(systemName: "arrow.down" ,withConfiguration: UIImage.SymbolConfiguration(pointSize: 4, weight: .regular, scale: .small
        ))?.withTintColor(.red, renderingMode:.alwaysOriginal)
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func tapPemasukan(_ sender: Any) {
        viewPemasukan.layer.borderColor = UIColor.systemIndigo.cgColor
        viewPemasukan.layer.borderWidth = 2
        tapped = true
        type = "Pemasukan"
        if inputJudul.text != "" && inputJumlah.text != "" {
        save.isEnabled = true
        save.layer.backgroundColor = UIColor.systemIndigo.cgColor
        }
        viewPengeluaran.layer.borderWidth = 0
    }
    
    @IBAction func tapPengeluaran(_ sender: Any) {
        viewPengeluaran.layer.borderColor = UIColor.systemIndigo.cgColor
        viewPengeluaran.layer.borderWidth = 2
        tapped = false
        type = "Pengeluaran"
        if inputJudul.text != "" && inputJumlah.text != "" {
        save.isEnabled = true
            save.layer.backgroundColor = UIColor.systemIndigo.cgColor
        }
        viewPemasukan.layer.borderWidth = 0
    }
    
    func random(length: Int) -> String {
        let id = "0123456789"
        return String((0..<length).map {_ in id.randomElement()! })
    }
    
    @IBAction func btnSimpan(_ sender: Any) {
        let id = "MM-\(random(length: 6))"
        let judul = inputJudul.text ?? ""
        let total = Int(inputJumlah.text ?? "") ?? 0
        let date = Date()

        let penggunaan = TableViewContents(id: id, contentsName: judul, contentsDate: date.toString(format: "dd MMMM yyyy - hh:mm"), contentsPrice: total, status: tapped, contentsType: type)
//        Penggunaan(penggunaan: penggunaan).add()

        //persistant data
//      UserDefaults.standard.set(try? PropertyListEncoder().encode(cellcontents), forKey: "savedArray")
        
        //from api
        BerandaNetworkService().add(data: penggunaan)
        self.showToast(message: "Data berhasil ditambahkan", font: .systemFont(ofSize: 12.0))
        DispatchQueue.main.asyncAfter(deadline: .now()+2){
            let viewController = BerandaViewController(nibName: String(describing: BerandaViewController.self), bundle: nil)
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

extension TambahPenggunaanNavigationController{
    func showToast(message : String, font: UIFont) {

        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-125, width: 200, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
             toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}

extension Date{
    func toString(format: String) -> String{
        let formatter = DateFormatter()
        formatter.timeZone = .current
        formatter.locale = .current
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}
