//
//  UbahPengguna.swift
//  MiddleProject
//
//  Created by Jeco Alviano on 14/05/21.
//`

import UIKit

class UbahPenggunaanNavigationController: /*UINavigationController*/ UIViewController {

    
    var service: BerandaNetworkService = BerandaNetworkService()

    @IBOutlet weak var imagePengeluaran: UIImageView!
    @IBOutlet weak var imagePemasukan: UIImageView!
    @IBOutlet weak var inputJudul: UITextField!
    @IBOutlet weak var inputJumlah: UITextField!
    @IBOutlet weak var save: UIButton!
    @IBOutlet weak var delete: UIButton!
    
    @IBOutlet weak var viewPengeluaran: UIView!
    @IBOutlet weak var viewPemasukan: UIView!
    
    
    var nama: String = ""
    var status: Bool = false
    var total: Int = 0
    
    var index: Int = 0
    
    var tapped : Bool = true
    var type : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tappedPemasukan = UITapGestureRecognizer(target: self, action: #selector(tapPemasukan(_:)))
        viewPemasukan.addGestureRecognizer(tappedPemasukan)
        
        let tappedPengeluaran = UITapGestureRecognizer(target: self, action: #selector(tapPengeluaran(_:)))
        viewPengeluaran.addGestureRecognizer(tappedPengeluaran)
        
        
        if status == false {
            self.tapPengeluaran(tappedPengeluaran)
        }
        else{
            self.tapPemasukan(tappedPemasukan)
        }

        save.layer.cornerRadius = 10
        
        delete.layer.cornerRadius = 10
        delete.layer.borderWidth = 1
        delete.layer.borderColor = UIColor.red.cgColor
        // Do any additional setup after loading the view.
        
        viewPemasukan.layer.shadowOpacity = 0.2
        viewPemasukan.layer.cornerRadius = 10
        
        viewPengeluaran.layer.shadowOpacity = 0.2
        viewPengeluaran.layer.cornerRadius = 10
        
        inputJudul.text = nama
        inputJumlah.text = String(total)
        
        
        imagePemasukan.image = UIImage(systemName: "arrow.up" ,withConfiguration: UIImage.SymbolConfiguration(pointSize: 4, weight: .regular, scale: .small
        ))
        imagePemasukan.tintColor = UIColor.green
        
        imagePengeluaran.image = UIImage(systemName: "arrow.down" ,withConfiguration: UIImage.SymbolConfiguration(pointSize: 4, weight: .regular, scale: .small
        ))
        imagePengeluaran.tintColor = UIColor.red
    }
    
    @IBAction func tapPemasukan(_ sender: Any) {
        viewPemasukan.layer.borderColor = UIColor.systemIndigo.cgColor
        viewPemasukan.layer.borderWidth = 2
        tapped = true
        type = "Pemasukan"
        if inputJudul.text != "" && inputJumlah.text != "" {
        save.isEnabled = true
        save.layer.backgroundColor = UIColor.systemIndigo.cgColor
        }
        viewPengeluaran.layer.borderWidth = 0
    }
    
    @IBAction func tapPengeluaran(_ sender: Any) {
        viewPengeluaran.layer.borderColor = UIColor.systemIndigo.cgColor
        viewPengeluaran.layer.borderWidth = 2
        tapped = false
        type = "Pengeluaran"
        if inputJudul.text != "" && inputJumlah.text != "" {
        save.isEnabled = true
            save.layer.backgroundColor = UIColor.systemIndigo.cgColor
        }
        viewPemasukan.layer.borderWidth = 0
    }

    @IBAction func btnHapus(_ sender: Any) {
        //cellcontents.remove(at: self.index)
        //UserDefaults.standard.removeObject(forKey: "savedArray")
        
        //using api
        let penggunaan = TableViewContents(id: String(self.index), contentsName: inputJudul.text, contentsDate: Date().toString(format: ""), contentsPrice: Int(inputJumlah.text ?? ""), status: tapped, contentsType: type)
        print("delete on",index)
        service.delete(id: String(index),data: penggunaan)
        //till here
        
        let viewController = BerandaViewController(nibName: String(describing: BerandaViewController.self), bundle: nil)
        self.navigationController?.setViewControllers([viewController], animated: true)
    }
    
    @IBAction func btnSimpan(_ sender: Any) {
        let judul = inputJudul.text ?? ""
        let total = Int(inputJumlah.text ?? "") ?? 0
        let tanggal = Date().toString(format: "")
       
//        cellcontents[index].contentsName = judul
//        cellcontents[index].contentsPrice = total
//        cellcontents[index].contentsDate = tanggal
//        cellcontents[index].status = tapped
//        cellcontents[index].contentsType = type
        
//        UserDefaults.standard.set(try? PropertyListEncoder().encode(cellcontents), forKey: "savedArray")
        
        //using api
        let penggunaan = TableViewContents(id: String(self.index), contentsName: judul, contentsDate: tanggal, contentsPrice: total, status: tapped, contentsType: type)
        service.edit(data: penggunaan, id: String(self.index))
        //till here
        
        let viewController = BerandaViewController(nibName: String(describing: BerandaViewController.self), bundle: nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
