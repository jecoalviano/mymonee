//
//  Impian.swift
//  MiddleProject
//
//  Created by Jeco Alviano on 13/05/21.
//

import Foundation

struct Impian {

    var lblImpian : String?
    var barProgress : Float?
    var lblAmount : Int?
    var lblTotal : Int?
}

//float between 0.0 and 1.0
var impian : [Impian] = [
//    Impian(lblImpian: "Membeli Mobil", barProgress: 0.12, lblAmount: 25000, lblTotal: 100000000),
//    Impian(lblImpian: "Membeli Airpods Baru", barProgress: 0.64, lblAmount: 1500000, lblTotal: 4000000),
//    Impian(lblImpian: "Membeli Sepatu Adidas", barProgress: 1.0, lblAmount: 5000000, lblTotal: 5000000)
]

