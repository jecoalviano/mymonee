//
//  ImpianViewController.swift
//  MiddleProject
//
//  Created by Jeco Alviano on 13/05/21.
//

import UIKit

class ImpianViewController: UIViewController {
 

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var notFoundImage: UIImageView!
    @IBOutlet weak var notFoundText: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.layer.cornerRadius = 10
        tableView.delegate = self
        tableView.dataSource = self
        notFoundImage.isHidden = true
        notFoundText.isHidden = true
        
        let uiNib = UINib(nibName: String(describing: ImpianCell.self), bundle: nil)
        tableView.register(uiNib, forCellReuseIdentifier: String(describing: ImpianCell.self))
    }

    @IBAction func btnAdd(_ sender: Any) {
        let add = TambahImpianNavigationController(nibName: String(describing: TambahImpianNavigationController.self), bundle: nil)
        self.navigationController?.pushViewController(add, animated: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }

    override func viewDidAppear(_ animated: Bool) {
        tableView.reloadData()
    }
}

extension ImpianViewController: UITableViewDelegate{
    func tableView(_ tableView:UITableView, didSelectRowAt indexPath: IndexPath){
        let viewController = LihatImpianViewController(nibName: String(describing: LihatImpianViewController.self), bundle: nil)
        
        viewController.index = indexPath.row
        
        viewController.amount = impian[indexPath.row].lblAmount ?? 0
        viewController.barprogress = impian[indexPath.row].barProgress ?? 0.0
        viewController.impian = impian[indexPath.row].lblImpian ?? ""
        viewController.totalbiaya = impian[indexPath.row].lblTotal ?? 0
        
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

extension ImpianViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if impian.count == 0{
            notFoundImage.isHidden = false
            notFoundText.isHidden = false
            tableView.isHidden = true
        }else
        {
            notFoundImage.isHidden = true
            notFoundText.isHidden = true
            tableView.isHidden = false
        }
        return impian.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ImpianCell.self), for: indexPath) as! ImpianCell
        
        cell.indextemp = indexPath.row
        
        let finish = UIImage(systemName: "checkmark.circle.fill")?.withConfiguration(UIImage.SymbolConfiguration(pointSize: 35, weight: .regular, scale: .default))
        let delete = UIImage(systemName: "trash.circle.fill")?.withConfiguration(UIImage.SymbolConfiguration(pointSize: 30, weight: .regular, scale: .default))
        
        
        if cell.impianProgress.progress >= 1.0 {
            cell.btnFinish.setImage(finish, for: .normal)
            cell.btnFinish.tintColor = UIColor.green
            cell.btnFinish.isEnabled = true
            
            cell.btnDelete.setImage(delete, for: .normal)
            cell.btnDelete.tintColor = UIColor.red
        }else
        {
            cell.btnFinish.setImage(finish, for: .disabled)
            cell.btnFinish.tintColor = UIColor.gray
            cell.btnFinish.isEnabled = false
            
            cell.btnDelete.setImage(delete, for: .normal)
            cell.btnDelete.tintColor = UIColor.red
        }
        cell.delegate = self
        cell.impianName.text = impian[indexPath.row].lblImpian
        cell.impianTotal.text = "IDR \(impian[indexPath.row].lblTotal ?? 0)"
        cell.impianAmount.text = "IDR \(impian[indexPath.row].lblAmount ?? 0)"
        cell.impianProgress.progress = impian[indexPath.row].barProgress ?? 0.0
        
        return cell
    }
}

extension ImpianViewController: handler{
    
    func delete(indexdata: Int) {
        let alert = UIAlertController(title: "Delete", message: "Hapus Impian?", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Hapus", style: .default, handler: {_ in impian.remove(at: indexdata); self.tableView.reloadData()}))
        alert.addAction(UIAlertAction(title: "Kembali", style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func finish(indexdata: Int) {
        let alert = UIAlertController(title: "Confirm", message: "Impian telah tercapai", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {_ in impian.remove(at: indexdata); self.tableView.reloadData()}))
        
        self.present(alert, animated: true, completion: nil)
    }
}
