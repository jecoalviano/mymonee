//
//  EditImpianViewController.swift
//  MiddleProject
//
//  Created by Jeco Alviano on 14/05/21.
//

import UIKit

class LihatImpianViewController: UIViewController {

    @IBOutlet weak var imgImpian: UIImageView!
    @IBOutlet weak var namaImpian: UILabel!
    @IBOutlet weak var biayaImpian: UILabel!
    @IBOutlet weak var save: UIButton!
    @IBOutlet weak var progressbar: UIProgressView!
    @IBOutlet weak var back: UIButton!
    @IBOutlet weak var biaya: UILabel!
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var progresscapaian: UIView!
    @IBOutlet weak var nilaiProgress: UILabel!
    
    var impian: String = ""
    var barprogress: Float = 0.0
    var amount: Int = 0
    var totalbiaya: Int = 0
    
    
    var index: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        namaImpian.text = impian
        biayaImpian.text = "\(amount)"
        biaya.text = "\(amount)"
        total.text = String(totalbiaya)
        progressbar.setProgress(barprogress, animated: true)
        nilaiProgress.text = "\(progressbar.progress) %"
        
        
        if progressbar.progress.isEqual(to: 1.0)
        {
            save.isEnabled = true
            save.layer.backgroundColor = UIColor.systemIndigo.cgColor
        }
        else{
            save.isEnabled = false
        }
        
        save.layer.cornerRadius = 10
        
        back.layer.cornerRadius = 10
        back.layer.borderWidth = 2
        back.layer.borderColor = UIColor.systemIndigo.cgColor
        
        
        progresscapaian.layer.cornerRadius = 10
        
        progressbar.layer.cornerRadius = 10
        
        imgImpian.image = UIImage(systemName: "heart.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 18, weight: .regular, scale: .medium))?.withTintColor(.red, renderingMode: .alwaysOriginal)
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func editButton(_ sender: Any) {
        let edit = UbahImpianNavigationController(nibName: String(describing: UbahImpianNavigationController.self), bundle: nil)
        
        edit.judul = impian
        edit.target = amount
        edit.index = index
        
        self.navigationController?.pushViewController(edit, animated: true)
    }
    
    func alert(){
        let dialogMessage = UIAlertController(title: "Confirm", message: "SELAMAT, ANDA SUDAH MENCAPAI TARGET", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            print("Ok button tapped")
         })
        dialogMessage.addAction(ok)
        self.present(dialogMessage, animated: true, completion: nil)
    }
    
    @IBAction func btnSimpan(_ sender: Any) {
        alert()
    }
    
    @IBAction func btnKembali(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
