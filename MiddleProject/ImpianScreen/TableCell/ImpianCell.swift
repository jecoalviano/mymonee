//
//  ImpianCell.swift
//  MiddleProject
//
//  Created by Jeco Alviano on 13/05/21.
//

import UIKit

protocol handler{
    func delete(indexdata: Int)
    func finish(indexdata: Int)
}

class ImpianCell: UITableViewCell {
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var impianProgress: UIProgressView!
    @IBOutlet weak var impianName: UILabel!
    @IBOutlet weak var impianAmount: UILabel!
    @IBOutlet weak var impianTotal: UILabel!
    @IBOutlet weak var btnFinish: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    var delegate: handler?
    var indextemp: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func finish(_ sender: Any) {
        delegate?.finish(indexdata: indextemp!)
    }
    
    @IBAction func hapus(_ sender: UIButton) {
        delegate?.delete(indexdata: indextemp!)
    }
    
    
    
}
