//
//  UbahImpianNavigationController.swift
//  MiddleProject
//
//  Created by Jeco Alviano on 14/05/21.
//

import UIKit

class TambahImpianNavigationController: /*UINavigationController*/ UIViewController  {

    @IBOutlet weak var textjudul: UITextField!
    @IBOutlet weak var textcapaian: UITextField!
    @IBOutlet weak var simpan: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        simpan.layer.cornerRadius = 10
        // Do any additional setup after loading the view.
    }

    
    @IBAction func btnSimpan(_ sender: Any) {
        let judul = textjudul.text ?? ""
        let total = Int(textcapaian.text ?? "") ?? 0
        
        let addImpian = Impian(lblImpian: judul, barProgress: 0.0, lblAmount: 0, lblTotal: total)
        impian.append(addImpian)
        let viewController = ImpianViewController(nibName: String(describing: ImpianViewController.self), bundle: nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
