//
//  UbahImpianNavigationController.swift
//  MiddleProject
//
//  Created by Jeco Alviano on 15/05/21.
//

import UIKit

class UbahImpianNavigationController: /*UINavigationController*/ UIViewController {

    @IBOutlet weak var inputJudul: UITextField!
    @IBOutlet weak var inputCapaian: UITextField!
    @IBOutlet weak var perbarui: UIButton!
    @IBOutlet weak var hapus: UIButton!
    
    var judul: String = ""
    var target: Int = 0
    
    var index: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        inputJudul.text = judul
        inputCapaian.text = String(target)
        

        perbarui.layer.cornerRadius = 10
        
        hapus.layer.cornerRadius = 10
        hapus.layer.borderWidth = 2
        hapus.layer.borderColor = UIColor.red.cgColor
        // Do any additional setup after loading the view.
    }

    func alert(){
        let dialogMessage = UIAlertController(title: "Confirm", message: "JUMLAH YANG TERKUMPUL TIDAK BISA MELEBIHI TARGET", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            print("Ok button tapped")
         })
        dialogMessage.addAction(ok)
        self.present(dialogMessage, animated: true, completion: nil)
    }

    
    @IBAction func btnPerbarui(_ sender: Any) {
        let judul = inputJudul.text ?? ""
        let target = Int(inputCapaian.text ?? "") ?? 0
        
        let targetBiaya: Float = Float(impian[index].lblTotal ?? 0)
        let terkumpul: Float = Float(target)

        if terkumpul > targetBiaya{
            alert()
        }
        else{
            let progressbar: Float = terkumpul/targetBiaya
            
            impian[index].lblImpian = judul
            impian[index].lblAmount = target
            impian[index].barProgress = progressbar
            
            let viewController = ImpianViewController(nibName: String(describing: ImpianViewController.self), bundle: nil)
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
    }
    
    @IBAction func btnHapus(_ sender: Any) {
        impian.remove(at: index.self)
        
        let viewController = ImpianViewController(nibName: String(describing: ImpianViewController.self), bundle: nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
