//
//  MainTabController.swift
//  MiddleProject
//
//  Created by Jeco Alviano on 12/05/21.
//

import UIKit

class MainTabController: UITabBarController {

    override func viewWillDisappear(_ animated: Bool) {
            super.viewWillAppear(animated)
            navigationController?.setNavigationBarHidden(true, animated: animated)
        }
    
    override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)

            self.navigationController?.setNavigationBarHidden(true, animated: animated)
        }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tabBar.barTintColor = .white
        
        
        //home
        let home = BerandaViewController(nibName: String(describing: BerandaViewController.self), bundle: nil)
        let homeTab = UINavigationController(rootViewController: home)
        homeTab.setNavigationBarHidden(true, animated: true)
        let homeImage = UIImage(systemName: "house", withConfiguration: UIImage.SymbolConfiguration(pointSize: 18, weight: .regular, scale: .medium))
        homeImage?.withTintColor(.lightGray, renderingMode: .alwaysTemplate)
        let homeImageSelected = UIImage(systemName: "house.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 18, weight: .regular, scale: .medium))
        let coloredHome = homeImageSelected?.withTintColor(.systemIndigo, renderingMode: .alwaysOriginal)
        homeTab.tabBarItem = UITabBarItem(title: "Home", image: homeImage, selectedImage: coloredHome)
        homeTab.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.lightGray], for: .normal)
        homeTab.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.systemIndigo], for: .selected)
        homeTab.tabBarItem.tag = 0
        
        
        //impian
        let impian = ImpianViewController(nibName: String(describing: ImpianViewController.self), bundle: nil)
        let impianTab = UINavigationController(rootViewController: impian)
        impianTab.setNavigationBarHidden(true, animated: false)
        let impianImage = UIImage(systemName: "heart", withConfiguration: UIImage.SymbolConfiguration(pointSize: 18, weight: .regular, scale: .medium))
        impianImage?.withTintColor(.lightGray, renderingMode: .alwaysTemplate)
        let impianImageSelected = UIImage(systemName: "heart.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 18, weight: .regular, scale: .medium))
        let coloredImpian  = impianImageSelected?.withTintColor(.systemIndigo, renderingMode: .alwaysOriginal)
        impianTab.tabBarItem = UITabBarItem(title: "Impian", image: impianImage, selectedImage: coloredImpian)
        impianTab.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.lightGray], for: .normal)
        impianTab.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.systemIndigo], for: .selected)
        impianTab.tabBarItem.tag = 1
        
        //profile
        
        let profile = EditProfileViewController(nibName: String(describing: EditProfileViewController.self), bundle: nil)
        let profileTab = UINavigationController(rootViewController: profile)
        profileTab.setNavigationBarHidden(true, animated: false)
        let profileImage = UIImage(systemName: "person", withConfiguration: UIImage.SymbolConfiguration(pointSize: 18, weight: .regular, scale: .medium))
        profileImage?.withTintColor(.lightGray, renderingMode: .alwaysTemplate)
        let profileImageSelected = UIImage(systemName: "person.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 18, weight: .regular, scale: .medium))
        let coloredProfile = profileImageSelected?.withTintColor(.systemIndigo, renderingMode: .alwaysOriginal)
        profileTab.tabBarItem = UITabBarItem(title: "Profile", image: profileImage, selectedImage: coloredProfile)
        profileTab.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.lightGray], for: .normal)
        profileTab.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.systemIndigo], for: .selected)
        profileTab.tabBarItem.tag = 2
        
        self.viewControllers = [homeTab, impianTab, profileTab]
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
