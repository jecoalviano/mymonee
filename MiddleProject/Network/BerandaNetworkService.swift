//
//  NetworkService.swift
//  MiddleProject
//
//  Created by MacBook on 20/05/21.
//

import Foundation

class BerandaNetworkService {
   
    
    func backendMonee(completion: @escaping (_ transaction: [TableViewContents]) -> ()) {
        let url: String = "https://60a5e470c0c1fd00175f4992.mockapi.io/api/v1/monee"
        let component = URLComponents(string: url)
        
        if let url = component?.url{
            let request = URLRequest(url: url)
            let task = URLSession.shared.dataTask(with: request) {
                (data, response, error) in
                if let data = data {
                    let decoder = JSONDecoder()
                    if let tableViewContents = try?
                        decoder.decode(Backend.self, from: data) as
                        Backend{
                        print(tableViewContents)
                        completion(tableViewContents.results ?? [])
                    }
                }
            }
            task.resume()
        }
    }
    
    
     func add(data: TableViewContents) {
         guard let url = URL(string: "https://60a5e470c0c1fd00175f4992.mockapi.io/api/v1/monee") else {
                    print("Error: cannot create URL")
                    return
                }
         
                // Convert model to JSON data
         guard let jsonData = try? JSONEncoder().encode(data) else {
                    print("Error: Trying to convert model to JSON data")
                    return
                }
                // Create the url request
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type") // the request is JSON
                request.setValue("application/json", forHTTPHeaderField: "Accept") // the response expected to be in JSON format
                request.httpBody = jsonData
                URLSession.shared.dataTask(with: request) { data, response, error in
                    guard error == nil else {
                        print("Error: error calling POST")
                        print(error!)
                        return
                    }
                    guard let data = data else {
                        print("Error: Did not receive data")
                        return
                    }
                    guard let response = response as? HTTPURLResponse, (200 ..< 299) ~= response.statusCode else {
                        print("Error: HTTP request failed")
                        return
                    }
                    do {
                        guard let jsonObject = try JSONSerialization.jsonObject(with: data) as? [String: Any] else {
                            print("Error: Cannot convert data to JSON object")
                            return
                        }
                        guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                            print("Error: Cannot convert JSON object to Pretty JSON data")
                            return
                        }
                        guard let prettyPrintedJson = String(data: prettyJsonData, encoding: .utf8) else {
                            print("Error: Couldn't print JSON in String")
                            return
                        }
                        
                        print(prettyPrintedJson)
                    } catch {
                        print("Error: Trying to convert JSON data to string")
                        return
                    }
                }.resume()
         
     }
     
    func edit(data: TableViewContents, id: String){
         guard let url = URL(string: "https://60a5e470c0c1fd00175f4992.mockapi.io/api/v1/monee/\(id ?? "")") else {
                    print("Error: cannot create URL")
                    return
                }
             
                // Add data to the model
 //               let uploadDataModel = UploadData(name: "Nicole", job: "iOS Developer")
                
                // Convert model to JSON data
                guard let jsonData = try? JSONEncoder().encode(data) else {
                    print("Error: Trying to convert model to JSON data")
                    return
                }
                 print(data)
                // Create the request
                var request = URLRequest(url: url)
                request.httpMethod = "PUT"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.httpBody = jsonData
                URLSession.shared.dataTask(with: request) { data, response, error in
                    guard error == nil else {
                        print("Error: error calling PUT")
                        print(error!)
                        return
                    }
                    guard let data = data else {
                        print("Error: Did not receive data")
                        return
                    }
                    guard let response = response as? HTTPURLResponse, (200 ..< 299) ~= response.statusCode else {
                        print("Error: HTTP request failed")
                        return
                    }
                    do {
                        guard let jsonObject = try JSONSerialization.jsonObject(with: data) as? [String: Any] else {
                            print("Error: Cannot convert data to JSON object")
                            return
                        }
                        guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                            print("Error: Cannot convert JSON object to Pretty JSON data")
                            return
                        }
                        guard let prettyPrintedJson = String(data: prettyJsonData, encoding: .utf8) else {
                            print("Error: Could print JSON in String")
                            return
                        }
                        
                        print(prettyPrintedJson)
                    } catch {
                        print("Error: Trying to convert JSON data to string")
                        return
                    }
                }.resume()
     }
     
    func delete(id : String,data: TableViewContents){
         guard let url = URL(string: "https://60a5e470c0c1fd00175f4992.mockapi.io/api/v1/monee/"+id) else {
                    print("Error: cannot create URL")
                    return
                }
             
         var request = URLRequest(url: url)
                request.httpMethod = "DELETE"
                URLSession.shared.dataTask(with: request) { data, response, error in
                    guard error == nil else {
                        print("Error: error calling DELETE")
                        print(error!)
                        return
                    }
                    guard let data = data else {
                        print("Error: Did not receive data")
                        return
                    }
                    guard let response = response as? HTTPURLResponse, (200 ..< 299) ~= response.statusCode else {
                        print("Error: HTTP request failed")
                        return
                    }
                    do {
                        guard let jsonObject = try JSONSerialization.jsonObject(with: data) as? [String: Any] else {
                            print("Error: Cannot convert data to JSON")
                            return
                        }
                        guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                            print("Error: Cannot convert JSON object to Pretty JSON data")
                            return
                        }
                        guard let prettyPrintedJson = String(data: prettyJsonData, encoding: .utf8) else {
                            print("Error: Could print JSON in String")
                            return
                        }
                        
                        print(prettyPrintedJson)
                    } catch {
                        print("Error: Trying to convert JSON data to string")
                        return
                    }
                }.resume()
         
         
     }
}
