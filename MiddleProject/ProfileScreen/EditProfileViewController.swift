//
//  EditProfileViewController.swift
//  MiddleProject
//
//  Created by Jeco Alviano on 13/05/21.
//

import UIKit
import Kingfisher

class EditProfileViewController: UIViewController{
    
    @IBOutlet weak var btnEditAndFinish: UIButton!
    @IBOutlet weak var btnImage: UIButton!
    @IBOutlet weak var btnName: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    var imagePicker = UIImagePickerController()
    var name: String = ""
    var picker = UIImagePickerController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblName.text = user[0].name
        
        profileImage.layer.cornerRadius = profileImage.frame.height/2
        profileImage.layer.masksToBounds = false
        profileImage.clipsToBounds = true
        profileImage.contentMode = .scaleAspectFit
        
        btnEditAndFinish.setTitle("Edit", for: .normal)
        
        //king fisher used
        let urlString = URL(string: "https://cdn.img.pink/images/2016/07/30/4k-image-tiger-jumping.jpg")
        profileImage.kf.setImage(with: urlString)
        //till here
        
        btnImage.isHidden = true
        btnImage.setImage(UIImage(systemName: "pencil.circle.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 28, weight: .regular, scale: .medium))?.withTintColor(.white, renderingMode: .alwaysOriginal), for: .normal)
        
        btnName.isHidden = true
        btnName.setImage(UIImage(systemName: "pencil", withConfiguration: UIImage.SymbolConfiguration(pointSize: 16, weight: .regular, scale: .medium))?.withTintColor(.white, renderingMode: .alwaysOriginal), for: .normal)
        // Do any additional setup after loading the view.
        
        if let imageData = UserDefaults.standard.object(forKey: "imageChanged") as? Data{
            profileImage.image = UIImage(data: imageData)
        }else{
            profileImage.image = UIImage(systemName: "person.circle")
        }
        
        profileImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(changeImage)))
    }
    
    
    func alert(){
        let dialogMessage = UIAlertController(title: "Confirm", message: "Nama tidak boleh kosong", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            print("Ok button tapped")
         })
        dialogMessage.addAction(ok)
        self.present(dialogMessage, animated: true, completion: nil)
    }
    
    @IBAction func editImage(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = true
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
  
    @IBAction func btnEditAndSave(_ sender: UIButton) {
        if sender.title(for: .normal) == "Edit"{
            btnEditAndFinish.setTitle("Selesai", for: .normal)
            btnImage.isHidden = false
            btnName.isHidden = false
        }
        else
        {
            btnEditAndFinish.setTitle("Edit", for: .normal)
            btnImage.isHidden = true
            btnName.isHidden = true
        }
    }
    
    @IBAction func editNama(_ sender: Any) {
        let dialogMessage = UIAlertController(title: "Title", message: "Isi dengan nama baru", preferredStyle: .alert)
        dialogMessage.addTextField { (textField) in
            textField.placeholder = ""
            textField.text = user[0].name
            }
        dialogMessage.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle cancel logic here")
        }))
        
        dialogMessage.addAction(UIAlertAction(title: "Ok", style: .default, handler: {(action: UIAlertAction!) in
            
            let name = dialogMessage.textFields![0] as UITextField
            user[0].name = name.text
            self.lblName.text = user[0].name
            
              print("Handle Ok logic here")
        }))


        present(dialogMessage, animated: true, completion: nil)
    }

}

extension EditProfileViewController: UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func changeImage(){
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            picker.delegate = self
            picker.sourceType = .savedPhotosAlbum
            picker.allowsEditing = false
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true){
            guard let image = info[.originalImage] as? UIImage else{fatalError()}
            UserDefaults.standard.set(image.pngData(), forKey: "imageChanged")
            self.profileImage.image = UIImage(data: image.pngData() ?? Data())
        }
    }
    

}
